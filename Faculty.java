package faculty;

/**
 *
 * @author Geanina Tambaliuc
 */
public class Faculty {

    private String name,position;
    private double salary;
    public Faculty(String name,String position,double salary)
    {
        this.name=name;
        this.position=position;
        this.salary=salary;
    }
    public Faculty(String name,double salary)
    {
        this.name=name;
        this.salary=salary;
    }
    public String getName()
    {
        return name;
    }
    public String getPosition()
    {
        return position;
    }
    public double getSalary()
    {
        return salary;
    }
    public void setName(String name)
    {
        this.name=name;
    }
    public void setPosition(String position)
    {
        this.position=position;
    }
    public void setSalary(double salary)
    {
        this.salary=salary;
    }
    public double calculateTax()
    {
        double tax=0;
        double s=this.salary;
        if(s<8500)
            tax=0.10*s;
        if(s>=8500 && s<34500)
            tax=s*0.15;
        if(s>=34500 && s<83600)
            tax=0.25*s;
        if(s>=83600 && s<174400)
            tax=0.28*s;
        if(s>=174400 && s<379150)
            tax=0.33*s;
        if(s>=379150)
            tax=0.35*s;
        return tax;
    }
    public double ninemonths()
    {
        double month9;
        month9=(this.salary-calculateTax())/9;
        return month9;
    }
    public double twelvemonths()
    {
        double month12;
        month12=(this.salary-calculateTax())/12;
        return month12;
    }
    public static void main(String[] args) {
        
        Faculty f1=new Faculty("John Doe", "Professor",47000);
        System.out.println("Monthly salary after tax: "+f1.twelvemonths());
        
      
    }
    
}
